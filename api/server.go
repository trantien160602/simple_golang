package api

import (
	"fmt"
	db "golang/db/sqlc"
	"golang/token"
	"golang/utils"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
)

type Server struct {
	config     utils.Config
	store      *db.SQLStore
	tokenMaker token.Maker
	router     *gin.Engine
}

func NewServer(config utils.Config, store *db.SQLStore) (*Server, error) {
	tokenMaker, err := token.NewJWTMaker(config.TokenSymmetricKey)
	if err != nil {
		return nil, fmt.Errorf("cannot create token maker: %w", err)
	}

	server := &Server{
		config:     config,
		store:      store,
		tokenMaker: tokenMaker,
	}

	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		v.RegisterValidation("currency", validCurrency)
	}

	server.routes()
	return server, nil
}

func (server *Server) Start(address string) error {
	return server.router.Run(address)
}

func errorResponse(err error, StatusCode int) Response {
	return Response{
		Success:     false,
		Message:     "",
		StatusCode:  StatusCode,
		MessageCode: err.Error(),
		DevMessage:  "",
		Data:        nil,
	}
}

type Response struct {
	Success     bool        `json:"success"`
	Message     string      `json:"message" default:""`
	StatusCode  int         `json:"status_code"`
	MessageCode string      `json:"message_code"`
	DevMessage  string      `json:"dev_message"`
	Data        interface{} `json:"data"`
}

type ResponsePageInfo struct {
	Success     bool        `json:"success"`
	Message     string      `json:"message" default:""`
	StatusCode  int         `json:"status_code"`
	MessageCode string      `json:"message_code"`
	DevMessage  string      `json:"dev_message"`
	Data        interface{} `json:"data"`
	PageInfo    interface{} `json:"page_info"`
}

func successResponse(result interface{}, status_code int) Response {
	return Response{
		Success:     true,
		Message:     "create success",
		Data:        result,
		StatusCode:  status_code,
		MessageCode: "",
		DevMessage:  "",
	}
}

func successResponsePageInfo(result interface{}, status_code int, PageInfo interface{}) ResponsePageInfo {
	return ResponsePageInfo{
		Success:     true,
		Message:     "create success",
		Data:        result,
		StatusCode:  status_code,
		MessageCode: "",
		DevMessage:  "",
		PageInfo:    PageInfo,
	}
}
