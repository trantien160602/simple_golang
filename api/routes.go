package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func (server *Server) routes() {
	// init router
	router := gin.Default()

	// user
	userRouter := router.Group("/users")
	{
		userRouter.POST("/", server.createUser)
		userRouter.POST("/login", server.loginUser)
	}

	//transfer
	transfersRouter := router.Group("/transfers")
	{
		transfersRouter.POST("", server.createTransfer)
	}

	//account
	accountRouter := router.Group("/accounts").Use(authMiddleware(server.tokenMaker))
	{
		accountRouter.POST("", server.createAccount)
		accountRouter.GET("/:id", server.getAccount)
		accountRouter.GET("", server.listAccounts)
	}

	// document API
	{
		router.Static("static", "./static/openapi")
		router.LoadHTMLFiles("templates/swagger/index.html")
		router.GET("/api-document", func(ctx *gin.Context) {
			ctx.HTML(http.StatusOK, "index.html", gin.H{})
		})
	}

	server.router = router
}
