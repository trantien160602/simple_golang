package utils

type Response struct {
	Success     bool        `json:"success"`
	Message     string      `json:"message"`
	Data        interface{} `json:"data"`
	StatusCode  int         `json:"status_code"`
	MessageCode string      `json:"message_code"`
	DevMessage  string      `json:"dev_message"`
	//PageInfo    interface{} `json:"page_info"`
}

// func successResponse(StatusCode int, result interface{}) gin.H {
// 	return gin.H{
// 		"success":      true,
// 		"message":      "create success",
// 		"message_code": "",
// 		"dev_message":  "",
// 		"status_code":  StatusCode,
// 		"data":         result,
// 	}
// }

func successResponse(result interface{}, message, message_code, dev_message string, status_code int) Response {
	return Response{
		Success:     true,
		Message:     message,
		Data:        result,
		StatusCode:  status_code,
		MessageCode: message_code,
		DevMessage:  dev_message,
	}
}
